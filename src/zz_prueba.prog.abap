REPORT zz_prueba.

PARAMETERS: p_tabnam TYPE tabname,
            p_selfl1 TYPE edpline,
            p_where1 TYPE edpline.

TYPES: r_type_fields TYPE RANGE OF name_feld.

DATA: lo_struct_type       TYPE REF TO cl_abap_structdescr,
      lo_dynamic_reference TYPE REF TO data,
      lo_result_table_type TYPE REF TO cl_abap_tabledescr.

DATA: lt_output_table_fields TYPE cl_abap_structdescr=>component_table,
      lt_split_fields_char   TYPE TABLE OF name_feld.

*SELECT
DATA: lt_select_list_fields_to_get TYPE TABLE OF edpline,
      lt_select_where              TYPE TABLE OF edpline,
      lv_table_name                TYPE tabname16.

DATA: r_get_fields TYPE r_type_fields.

DATA: lv_index TYPE int4.

FIELD-SYMBOLS: <fs_output_table_fields> TYPE abap_componentdescr,
               <fs_split_fields_char>   TYPE name_feld,
               <fs_r_get_fields>        LIKE LINE OF r_get_fields,
               <lt_result_table>        TYPE ANY TABLE.

* Obtenemos la estructura general de la tabla
lo_struct_type ?= cl_abap_typedescr=>describe_by_name( p_tabnam ).

* Armamos un rango con los campos que se quieren obtener de la tabla
SPLIT p_selfl1 AT space INTO TABLE lt_split_fields_char.

REFRESH r_get_fields.

APPEND INITIAL LINE TO r_get_fields ASSIGNING <fs_r_get_fields>.
<fs_r_get_fields>-sign = 'I'. <fs_r_get_fields>-option = 'EQ'.
<fs_r_get_fields>-low = '999999999'.

LOOP AT lt_split_fields_char ASSIGNING <fs_split_fields_char>.

  APPEND INITIAL LINE TO r_get_fields ASSIGNING <fs_r_get_fields>.
  <fs_r_get_fields>-sign = 'I'. <fs_r_get_fields>-option = 'EQ'.
  <fs_r_get_fields>-low = <fs_split_fields_char>.

ENDLOOP.

* Nos quedamos solo con los campos que se pidio obtener
PERFORM get_components USING lo_struct_type
                             r_get_fields
                       CHANGING lt_output_table_fields.

* Creamos una estructura con los campos que se pidieron obtener
lo_struct_type = cl_abap_structdescr=>create( lt_output_table_fields ).
lo_result_table_type   = cl_abap_tabledescr=>create( lo_struct_type ).

* Creamos una tabla interna con esa estructura
CREATE DATA lo_dynamic_reference TYPE HANDLE lo_result_table_type.
ASSIGN lo_dynamic_reference->* TO <lt_result_table>.

* Creation of the selection fields
APPEND p_selfl1 TO lt_select_list_fields_to_get.

* Creation of the "where" clause
APPEND p_where1 TO lt_select_where.

lv_table_name = p_tabnam.

BREAK-POINT.

* Realizamos el Select dinámico
SELECT   (lt_select_list_fields_to_get)
 FROM    (lv_table_name)
 INTO CORRESPONDING FIELDS OF TABLE <lt_result_table>
 WHERE    (lt_select_where).

* Cambio

BREAK-POINT.
*&---------------------------------------------------------------------*
*&      Form  GET_COMPONENTS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LO_STRUCT_TYPE  text
*      -->P_r_get_fields  text
*      <--P_LT_OUTPUT_TABLE_FIELDS  text
*----------------------------------------------------------------------*
FORM get_components  USING    po_struct_type TYPE REF TO cl_abap_structdescr
                              pr_get_fields TYPE r_type_fields
                     CHANGING pt_output_table_fields TYPE cl_abap_structdescr=>component_table.

  DATA: lo_struct_type TYPE REF TO cl_abap_structdescr.

  DATA: lt_table_fields TYPE cl_abap_structdescr=>component_table.

  DATA: lv_index TYPE int4.

  FIELD-SYMBOLS: <fs_table_fields>        TYPE abap_componentdescr,
                 <fs_output_table_fields> TYPE abap_componentdescr.

  lt_table_fields = po_struct_type->get_components( ).

  LOOP AT lt_table_fields ASSIGNING <fs_table_fields>.

    lv_index = sy-tabix.


    IF <fs_table_fields>-as_include = 'X'.
* Si tiene estructuras append, buscamos los campos que necesitamos de esas estructuras.

      lo_struct_type ?= <fs_table_fields>-type.

      PERFORM get_components USING lo_struct_type
                                   pr_get_fields
                             CHANGING pt_output_table_fields.

    ELSEIF <fs_table_fields>-name IN r_get_fields.
      APPEND INITIAL LINE TO pt_output_table_fields ASSIGNING <fs_output_table_fields>.
      <fs_output_table_fields> = <fs_table_fields>.

    ENDIF.

  ENDLOOP.

ENDFORM.
