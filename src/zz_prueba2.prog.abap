*&---------------------------------------------------------------------*
*& Report ZZ_PRUEBA2
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zz_prueba2.

DATA: dsn(200)  TYPE c,
      rec(400) TYPE c.

DATA: lt_file  TYPE STANDARD TABLE OF string.

FIELD-SYMBOLS: <fs_file> TYPE string.

dsn = 'C:\Users\JimenaInesNosei\Desktop\Credito y caucion\S0042517.csv'.

OPEN DATASET dsn FOR INPUT IN TEXT MODE ENCODING DEFAULT.

IF sy-subrc = 0.

  DO.

    READ DATASET dsn INTO rec.

    IF sy-subrc <> 0.

      EXIT.

    ELSE.

      APPEND INITIAL LINE TO lt_file ASSIGNING <fs_file>.
      <fs_file> = rec.

    ENDIF.

  ENDDO.

ENDIF.

CLOSE DATASET dsn.

BREAK-POINT.
